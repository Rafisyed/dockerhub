# My project's README

Step 1 : Install Docker (https://docs.docker.com/docker-for-windows/install/) & Clone the 
         project 

Step 2 : get into project dir "cd DockerHub"

Step 3 : Build the docker image from docker file "docker build -t kafka:2.11-0.10.2.0 ." 
         Make sure "Dockerfile" is available at the DockerHub directory

Step 4 : Once the image is built start the kafka server/run the image
	 "docker run -p 2181:2181 -p 9092:9092 --name kafka-quickstart -d kafka:2.11-0.10.2.0"